import numpy as np
import matplotlib.pyplot as plt
import sys
from cauc import cauc



quiet=False
if len(sys.argv)>1:
  quiet=sys.argv[1]=="quiet"

index=None
if len(sys.argv)>2:
  index=int(sys.argv[2])



if type(index)==type(None):
  f=np.load("evalb.npz")
else:
  f=np.load("multi/"+str(index)+"/evalb.npz")

x=f["x"]
y=f["y"]
p=f["p"]
c=f["c"]

#print(y)

#exit()


q=cauc(p=p,c=c,y=y)

auc=q["auc"]
i30=q["i30"]
e30=q["e30"]
tpr=q["tpr"]
fpr=q["fpr"]
d0=q["d0"]
d1=q["d1"]


print("auc:",auc,"   at",tpr[i30],":",e30,"=1/",1/e30)
#exit()

#tprs=1-tprs
#fprs=1-fprs

#plt.plot(fprs,tprs,color="red",alpha=0.5)
#plt.plot(tprs,1/(fprs+0.00001),"o")
#plt.yscale("log",nonposy="clip")
#plt.show()


#exit()








if type(index)==type(None):
  np.savez_compressed("roc",**q)
else:
  np.savez_compressed("multi/"+str(index)+"/"+"roc",**q)


minx=np.min([np.min(d0),np.min(d1)])
maxx=np.max([np.max(d0),np.max(d1)])
rang=[minx,maxx]

#plt.hist(d,bins=200)
plt.hist(d0,bins=100,alpha=0.5,label="0",range=rang,color="orange")
plt.hist(d1,bins=100,alpha=0.5,label="1",range=rang,color="blue")

plt.legend()

plt.xlabel("mse")
plt.ylabel("#")

if type(index)==type(None):plt.savefig("imgs/recqual.png",format="png")
if type(index)==type(None):plt.savefig("imgs/recqual.pdf",format="pdf")

if not quiet:plt.show()

plt.close()


plt.plot(fpr,tpr,label="auc="+str(round(auc,4)),color="darkblue")
plt.plot(tpr,fpr,label="auc="+str(round(1-auc,4)),color="dodgerblue")

plt.plot(np.arange(0,1,0.01),np.arange(0,1,0.01),color="grey",alpha=0.3)



plt.legend()
plt.xlabel("false positive rate (is true, but classified as false)")
plt.ylabel("true positive rate (is true, and classified as true)")

if type(index)==type(None):plt.savefig("imgs/roc.png",format="png")
if type(index)==type(None):plt.savefig("imgs/roc.pdf",format="pdf")
if not quiet:plt.show()


















