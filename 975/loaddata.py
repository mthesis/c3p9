import numpy as np

compareto=""
dataset="top/qcd"
from trainingon import *
from ageta import n,gs,vn

def load_tq_train(subset):
  f=np.load("..//..//toptagdataref//train"+subset+".npz")
  x=f["xb"][:n,:gs,:]
  #y=f["y"][:n]
  del f
  return x


def relu(x):
  return (x+np.abs(-x))/2

def heavi(x):
  C=10000
  return relu(C*x)-relu(C*x-1)


def load_tq_trainplus(subset):
  f=np.load("..//..//toptagdataref//train"+subset+".npz")
  x=f["xb"][:n,:,:]
  cou=np.sum(heavi(np.sum(np.abs(x),axis=-1)),axis=-1)
  

  x=x[:,:gs,:]
  #y=f["y"][:n]
  del f
  return x,cou

def load_dm_train(subset):
  f=np.load("..//..//newdata3//train"+subset+".npz")
  x=f["x"][:n,:gs,:]
  #y=f["y"][:n]
  del f
  return x

def load_gq_train(subset):
  f=np.load("..//..//rdata//"+subset+".npz")
  q=f["q"]

  l=len(q)
  q=q[:int(l*0.8)]

  del f
  x=q[:n,:gs,:]
  return x

def load_tq_valplus(subset):
  vf=np.load("..//..//toptagdataref//val"+subset+".npz")
  vx=vf["xb"][:vn,:,:]

  cou=np.sum(heavi(np.sum(np.abs(vx),axis=-1)),axis=-1)

  

  vx=vx[:,:gs,:]


  vy=vf["y"][:vn]
  del vf
  return vx,vy,cou

def load_tq_val(subset):
  vf=np.load("..//..//toptagdataref//val"+subset+".npz")
  vx=vf["xb"][:vn,:gs,:]
  vy=vf["y"][:vn]
  del vf
  return vx,vy

def load_dm_val(subset):
  vf=np.load("..//..//newdata3//val"+subset+".npz")
  vx=vf["x"][:vn,:gs,:]
  vy=vf["y"][:vn]
  del vf
  return vx,vy

def load_gq_val(subset):
  f=np.load("..//..//rdata//"+subset+".npz")
  q=f["q"]

  l=len(q)
  q=q[int(l*0.8):]

  del f
  x=q[:n,:gs,:]
  return x
  


def load_gq_both(subset,altset):
  fT=np.load("..//..//rdata//"+subset+".npz")
  xT=fT["q"][:vn,:gs,:]
  fF=np.load("..//..//rdata//"+altset+".npz")
  xF=fF["q"][:vn,:gs,:]

  ml=np.min([len(xT),len(xF)])
  xT=xT[:ml]
  xF=xF[:ml]
 
  yT=np.zeros(len(xT))
  yF=np.ones(len(xF))

  return np.concatenate((xT,xF),axis=0),np.concatenate((yT,yF),axis=0)


def load_tq_evalplus():
  return load_tq_valplus("_fairer")

def load_tq_eval():
  return load_tq_val("_fairer")

def load_dm_eval():
  return load_dm_val("_fairer")

def load_gq_eval(subset,altset):
  bx,by=load_gq_both(subset,altset)
  l=len(by)
  mindex=int(l/2)-5000
  maxdex=int(l/2)+5000

  return bx[mindex:maxdex],by[mindex:maxdex]


def load_tq_bigeval():
  return load_tq_val(str(trainingon))

def load_dm_bigeval():
  return load_dm_val(str(trainingon))

def load_gq_bigeval(subset,altset):
  return load_gq_both(subset,altset)


def filterset(q):
  return q.replace("_short","").replace("_fair","").replace("_fairer","")


def loadtrainplus(subset=None):
  if subset is None:subset=trainingon
  if dataset=="top/qcd":
    return load_tq_trainplus(subset)
  print("not implemented")
  exit()

def loadtrain(subset=None):
  if subset is None:subset=trainingon
  if dataset=="top/qcd":
    return load_tq_train(subset)
  if dataset=="dm":
    return load_dm_train(filterset(subset))
  if dataset=="gq":
    return load_gq_train(filterset(subset))


def loadval(subset=None):
  if subset is None:subset=trainingon
  if dataset=="top/qcd":
    return load_tq_val(subset)
  if dataset=="dm":
    return load_dm_val(filterset(subset))
  if dataset=="gq":
    return load_gq_val(filterset(subset))

def loadevalplus(subset=None,altset=None):
  if subset is None:subset=trainingon
  if altset is None:altset=compareto
  if dataset=="top/qcd":
    return load_tq_evalplus()
  print("not implemented")
  exit()

def loadeval(subset=None,altset=None):
  if subset is None:subset=trainingon
  if altset is None:altset=compareto
  if dataset=="top/qcd":
    return load_tq_eval()
  if dataset=="dm":
    return load_dm_eval()
  if dataset=="gq":
    return load_gq_eval(subset,altset)

def loadbigeval(subset=None,altset=None):
  if subset is None:subset=trainingon
  if altset is None:altset=compareto
  if dataset=="top/qcd":
    return load_tq_bigeval()
  if dataset=="dm":
    return load_dm_bigeval()
  if dataset=="gq":
    return load_gq_bigeval(subset,altset)




if __name__=="__main__":
  dataset="dm"
  trainingon="_au3"
  compareto="_au4"
  x,y=loadbigeval()

  print(x.shape,y.shape) 




















