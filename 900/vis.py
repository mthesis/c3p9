import numpy as np
import json
import matplotlib.pyplot as plt


try:
  from trainingon import *
except:trainingon="_au0"


with open("model.json","r") as f:
  m=json.loads(f.read())

def realtype(t):
  t=t[8:]
  t=t[:-2]
  while "." in t:
    t=t[t.find(".")+1:]
  return t

jobs=[]

jobs.append({"t":"typ","what":trainingon})

todraw=("gpre5","gssort","gpartinorm","BatchNormalization","glm","gcompool","gcomdepoolplus","glm",   "gtopk","gcomfullyconnected","gcomparamlevel","gltknd","gcomdepool")


#prefilter layers for drawing
for sm in m.keys():
  for l in m[sm]:
    t=realtype(l["type"])
    if not t in todraw:
      print("not",t)
      continue
    if t=="gcomfullyconnected":
      if l["gs"]==1:continue
    l.update({"t":t})
    jobs.append(l)
    print(t)

# print(jobs)



#each job has x dimension of 1

#add_artist,add_patch

def drawtext(x0,y0,x1,y1,t,c):
  #not yet good, just placeholder
  # tex=plt.text((x0+x1)/2-(x1-x0)*0.2,(y0+y1)/2,t,color=c)
  tex=plt.text((x0+x1)/2-(x1-x0)*0.0,(y0+y1)/2,t,ha="center",va="center",size=20)
  # print(dir(tex.figure))
  # exit()
  # plt.gca().add_artist(tex)

def drawline(x0,y0,x1,y1,c):
  # line=plt.Line2D((x0,x1),(y0,y1),lw=2.5,color=c,alpha=0.5)
  line=plt.Line2D((x0,x1),(y0,y1),lw=2.5,color="black")
  plt.gca().add_line(line)  

def drawrectangle(x0,y0,x1,y1,c):
  drawline(x0,y0,x1,y0,c)
  drawline(x0,y0,x0,y1,c)
  drawline(x1,y1,x1,y0,c)
  drawline(x1,y1,x0,y1,c)

def drawcircle(x,y,r):
  circle=plt.Circle((x,y),r, fill=False)
  plt.gca().add_patch(circle)

def drawparral(x0,x1,a0,a1,b0,b1,c):
  drawline(x0,a0,x1,b0,c)
  drawline(x0,a1,x1,b1,c)
  drawline(x0,a0,x0,a1,c)
  drawline(x1,b0,x1,b1,c)

def drawgpre5(x0,y0,y1,job):

  texs=["flag","phi","eta","lpt"]

  # drawtext(0,0,"Hello")

  dy=(y1-y0)/4
  for i in range(4):
    drawrectangle(x0,y0+i*dy,x0+1,y0+(i+1)*dy,c="grey")
    drawtext(x0,y0+i*dy,x0+1,y0+(i+1)*dy,texs[3-i],c="grey")

  return True

def drawsort(x0,y0,y1,job):
  drawrectangle(x0,y0,x0+1,y1,c="cyan")
  
  # drawtext(x0,y0,x0+1,y1,"sort",c="cyan")
  
  dy=y1-y0
  
  n=3
  for i in range(n):
    drawrectangle(x0,y0+dy*(i/n),x0+1,y0+dy*((i+1)/n),c="cyan")
    drawtext(x0,y0+dy*(i/n),x0+1,y0+dy*((i+1)/n),str(n-i),c="cyan")
  
  # drawtext(x0,y0,x0+1,y1,"sort",c="cyan")
  
  return True

def drawpnorm(x0,y0,y1,job):
  drawrectangle(x0,y0,x0+1,y1,c="red")
  dy=y1-y0
  # drawline(x0+0.2,y0+dy*0.2,x0+0.8,y1-dy*0.6,c="red")
  # drawline(x0+0.2,y0+dy*0.6,x0+0.8,y1-dy*0.2,c="red")
  for m in [0.1,0.3,0.5,0.7]:
    drawline(x0+0.2,y0+dy*m,x0+0.4,y0+dy*(m+0.2),c="red")
    drawline(x0+0.6,y0+dy*m,x0+0.8,y0+dy*(m+0.2),c="red")
  
  return True
  
def drawbatchnorm(x0,y0,y1,job):
  drawrectangle(x0,y0,x0+1,y1,c="orange")
  dy=y1-y0
  drawline(x0+0.2,y0+dy*0.2,x0+0.8,y1-dy*0.2,c="orange")
  
  return True
  
def drawglm(x0,y0,y1,job):
  
  dy=(y1-y0)/2

  x=[x0+0.33,x0+0.65,x0+0.65]
  y=[(y0+y1)/2-dy*0.2,(y0+y1)/2+dy*0.6,(y0+y1)/2-dy*0.6]
  r=[0.25 for yy in y]

  for xx,yy,rr in zip(x,y,r):
    drawcircle(xx,yy,rr)
  
  for i in range(len(x)):
    for j in range(i+1,len(x)):
      xa=x[i]
      xb=x[j]
      ya=y[i]
      yb=y[j]
      ra=r[i]
      rb=r[j]
      
      dx=xb-xa
      dy=yb-ya
      
      phi=np.arctan2(dy,dx)
      
      
      drawline(xa+np.cos(phi)*ra,ya+np.sin(phi)*ra,xb-np.cos(phi)*rb,yb-np.sin(phi)*rb,c="black")
  
  # drawline(
  
  drawrectangle(x0,y0,x0+1,y1,c="black")
  
  return True

def drawfullyconnected(x0,y0,y1,job):
  
  if int(job["gs"])==1:return False
  
  dy=(y1-y0)/2

  x=[x0+0.2,x0+0.2,x0+0.8,x0+0.8]
  y=[(y0+y1)/2-dy*0.4,(y0+y1)/2+dy*0.4,(y0+y1)/2-dy*0.4,(y0+y1)/2+dy*0.4]
  r=[0.15 for yy in y]

  for xx,yy,rr in zip(x,y,r):
    drawcircle(xx,yy,rr)
  
  for i in range(len(x)):
    for j in range(i+1,len(x)):
      xa=x[i]
      xb=x[j]
      ya=y[i]
      yb=y[j]
      ra=r[i]
      rb=r[j]
      
      dx=xb-xa
      dy=yb-ya
      
      phi=np.arctan2(dy,dx)
      
      
      drawline(xa+np.cos(phi)*ra,ya+np.sin(phi)*ra,xb-np.cos(phi)*rb,yb-np.sin(phi)*rb,c="black")
  
  # drawline(
  
  drawrectangle(x0,y0,x0+1,y1,c="black")
  
  return True

def drawpool(x0,y0,y1,job):
  dy=y1-y0
  drawparral(x0,x0+1,y0,y1,y0+dy*0.2,y1-dy*0.2,"black")
  
  return True
def drawdepoolplus(x0,y0,y1,job):
  dy=y1-y0
  drawparral(x0,x0+1,y0,y1,y0-dy/3,y1+dy/3,"black")
  
  return True

def drawtopk(x0,y0,y1,job):
  drawrectangle(x0,y0,x0+1,y1,c="black")
  
  dy=y1-y0
  
  drawline(x0+0.5,y0+dy*0.4,x0+0.5,y1-dy*0.4,c="yellow")
  drawline(x0+0.5,y0+dy*0.4,x0+0.3,y1-dy*0.55,c="yellow")
  drawline(x0+0.5,y0+dy*0.4,x0+0.7,y1-dy*0.55,c="yellow")
  

  
  
  x=[x0+f for f in [0.3,0.5,0.7]]
  y=[y0+dy*f for f in [0.2,0.3,0.1]]
  y2=[yy+0.6*dy for yy in y]
  r=[0.15 for yy in y]

  for xx,yy,rr in zip(x,y,r):
    drawcircle(xx,yy,rr)
  for xx,yy,rr in zip(x,y2,r):
    drawcircle(xx,yy,rr)
  
  for i in range(len(x)):
    for j in range(i+1,len(x)):
      xa=x[i]
      xb=x[j]
      ya=y[i]
      yb=y[j]
      ra=r[i]
      rb=r[j]
      
      dx=xb-xa
      dy=yb-ya
      
      phi=np.arctan2(dy,dx)
      
      
      drawline(xa+np.cos(phi)*ra,ya+np.sin(phi)*ra,xb-np.cos(phi)*rb,yb-np.sin(phi)*rb,c="black")
  
  
  
  
  
  return True

def drawconn(x0,y0,x1,y1):
  drawline(x0,y0,x0,y1,c="red")
  drawline(x1,y0,x1,y1,c="blue")
  drawline(x0,y1,x1,y1,c="green")

def drawtyp(x0,y0,y1,job):
  drawrectangle(x0,y0,x0+1,y1,c="black")
  
  
  q=job["what"]
  
  mp={"_au0":"qcd","_au1":"top","_au3":"lqcd","_au4":"ldm","quark":"quark","gluon":"gluon"}
  
  t=mp[q]+" jets"
  
  
  
  tex=plt.text((x0+0.5),(y0+y1)/2,t,ha="center",va="center",size=20,rotation=90,color="black")

  return True
  
  

def draw(t,job,x0,y0,y1):
  jobs={"gpre5":drawgpre5,
  "gssort":drawsort,
  "gpartinorm":drawpnorm,
  "BatchNormalization":drawbatchnorm,
  "gltknd":drawglm,
  "glm":drawglm,
  "gcompool":drawpool,
  "gcomdepool":drawdepoolplus,
  "gcomdepoolplus":drawdepoolplus,
  "gcomfullyconnected":drawfullyconnected,
  "gtopk":drawtopk,
  "typ":drawtyp,
  "gcomparamlevel":drawdepoolplus}
  if t in jobs.keys():
    return jobs[t](x0,y0,y1,job)
    
    
  drawrectangle(x0,y0,x0+1,y1,c="white")
  return True
    
  return False

def adapty(y0,y1,t,job):
  if t=="gcompool":
    dy=y1-y0
    y0+=0.2*dy
    y1-=0.2*dy
  if t=="gcomdepoolplus" or t=="gcomparamlevel" or t=="gcomdepool":
    dy=y1-y0
    y0-=dy/3
    y1+=dy/3
  
  return y0,y1

x=0
y0=0
y1=len(jobs)

y0o=y0
y1o=y1

plt.figure(figsize=(20*len(jobs)/(y1-y0),10))

xs=0

for job in jobs:
  if draw(job["t"],job,x,y0,y1):
    x+=1
    y0,y1=adapty(y0,y1,job["t"],job)
  if job["t"]=="BatchNormalization":xs=x-1
  if job["t"]=="gpartinorm":xs=x
# plt.axis("scale")


dyo=y1o-y0o

drawconn(xs,y1o,x,y1o+dyo*0.05)


plt.xlim([-0.5,x+0.5])
plt.ylim([y0o-dyo*0.1,y1+dyo*0.1])

plt.axis("off")

plt.savefig("imgs/vis.png",format="png")
plt.savefig("imgs/vis.pdf",format="pdf")


plt.show()




