import numpy as np
import matplotlib.pyplot as plt
import csv
from os import listdir
from os.path import isfile

def read(q):
  with open(q,"r") as f:
    return f.read()

def readspec(q=""):
  c=read(str(q)+"spec.py")
  c=eval(c[5:])
  return c

def advreadloss(q):
  with open(str(q)+"history.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        try:
          q[i].append(float(row[i]))
        except:
          q[i].append(1000000.0)

  for i in range(1,len(q)):
    if qn[i]=="val_loss":return np.min(q[i])
  return 0.0

def mindex():
  multis=[f for f in listdir("multi") if not isfile(f)]
  losses=[]
  indices=[]
  for m in multis:
    losses.append(advreadloss("multi/"+m+"/"))
    indices.append(m)
  ind=indices[np.argmin(losses)]
  index=int(ind)

  acl=np.min(losses)
  comp=advreadloss("")
  if comp<acl:index=-1
  return index

def alllosses():
  multis=[f for f in listdir("multi") if not isfile(f)]
  losses=[]
  indices=[]
  for m in multis:
    losses.append(advreadloss("multi/"+m+"/"))
    indices.append(m)

  comp=advreadloss("")
  losses.append(comp)
  indices.append(-1)
  return indices,losses

def readroc(q=""):

  ret=[]
  ret.append(np.load(str(q)+"roc.npz")["auc"])
  for i in range(1,1000):
    try:
      ret.append(np.load(str(q)+f'multi/{i}/roc.npz')["auc"])
    except:
      ret.append(0)
      break
  #print(len(ret))
  return ret



def readloss(q=""):

  ret=[]
  ret.append(np.load(str(q)+"data/loss.npz")["q"])
  for i in range(1,1000):
    try:
      ret.append(np.load(str(q)+f'multi/{i}/loss.npz')["q"])
    except:
      ret.append(0)
      break
  return ret
def readadvloss(q):

  ret=[]
  ret.append(advreadloss(q+"/"))
  for i in range(1,1000):
    try:
      ret.append(advreadloss(str(q)+f'/multi/{i}/'))
    except:
      ret.append(0)
      break
  return ret


if __name__=="__main__":
  l=readloss(q="")
  l2=readadvloss(q="")
  a=readroc(q="")

  np.savez_compressed("data/quality",loss=l,aloss=l2,auc=a)


