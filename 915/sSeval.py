import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *



from createmodel import objects as o
from adv2load import *
from ageta import *

import sys

from sep import getmodel, base

from loaddata import *

#exit()

f=np.load("bcode.npz")
x,y,p=f["x"],f["y"],f["p"]
del f


altp=loadevalplus()

#altp=np.reshape(altp,(len(altp),1))

p=altp

#print(p.shape,altp.shape)
#exit()



mul=int(p.shape[-1])

#y=keras.utils.to_categorical(y,2)

index=None
try:
  if len(sys.argv)>1:index=int(sys.argv[1])
except:pass

id=1
if len(sys.argv)>2:id=int(sys.argv[2])


print(f"Working on {index}  {id}")

#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               

mis=["b"]#,"a"]

#if len(sys.argv)>1:
#  cut=int(sys.argv[1])



#  mis=[["b","a"][cut]]

for mi in mis:
  model=getmodel(list([ac*mul for ac in base]))
  if type(index)==type(None):
    model.load_weights(f"msSep{id}.tf")
  else:
    model.load_weights(f"multi/{index}/msSep{id}.tf")

  model.summary()





  my=model.predict(p,verbose=1)
  if type(index)==type(None):
    np.savez_compressed(f"sSeval{id}",x=x,y=y,p=p,s=my)
  else:
    np.savez_compressed("multi/"+str(index)+f"/sSeval{id}",x=x,y=y,p=p,s=my)



