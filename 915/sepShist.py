import numpy as np
import matplotlib.pyplot as plt



f=np.load("sepSout.npz")
tpr=f["tpr"]
fpr=f["fpr"]

d=f["d"]
y=f["y"]

sy=set(y)

for ay in sy:
  plt.hist(d[np.where(y==ay)],alpha=0.5,label=str(ay),bins=100)

plt.legend()

plt.show()


