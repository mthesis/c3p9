import numpy as np
import matplotlib.pyplot as plt


invert=True




f=np.load("evalb.npz")

x=f["x"]
y=f["y"]
p=f["p"]
c=f["c"]




f2=np.load("evalsvb.npz")

x2=f2["x"]
y2=f2["y"]
p2=f2["p"]
c2=f2["c"]

#print(y)

#exit()

def difference(a,b):
  #print(a.shape,b.shape,np.mean(a-b).shape)
  #exit()
  return np.sqrt(np.mean((a-b)**2))

d=np.zeros((len(y),))

d0,d1=[],[]

for i in range(len(y)):
  d[i]=difference(c[i],p[i])
  if (y[i]>0.5) != invert:
#  if np.random.randint(2)==0:
    d1.append(d[i])
  else:
    d0.append(d[i])

d2=[]
for i in range(len(y2)):
  d2.append(difference(c2[i],p2[i]))


plt.hist(d0,bins=100,alpha=0.5,label="0")
plt.hist(d1,bins=100,alpha=0.5,label="1")
plt.hist(d2,bins=100,alpha=0.5,label="2")

plt.legend()

plt.xlabel("mse")
plt.ylabel("#")

plt.savefig("imgs/svrecqual.png",format="png")
plt.savefig("imgs/svrecqual.pdf",format="pdf")

plt.show()

plt.close()
