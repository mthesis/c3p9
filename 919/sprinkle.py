import numpy as np


def relu(x):
  return (x+np.abs(-x))/2

def heavi(x):
  C=10000
  return relu(C*x)-relu(C*x-1)


def geteta(x):
  p=np.sqrt(x[:,:,1]**2+x[:,:,2]**2+x[:,:,3]**2)
  p3=x[:,:,3]

  eta=0.5*np.log(0.00000001+(p+p3)/(p-p3+0.000000001))
  return eta

def getphi(x):
  p=np.sqrt(x[:,:,1]**2+x[:,:,2]**2+x[:,:,3]**2)
  p3=x[:,:,3]

  phi=np.arctan2(x[:,:,2],x[:,:,1])
  return phi



def sprinkle(x,gs):
  """addon function for additional parameters for sepP, needs to return an array of (same lengsth as x, ?)"""

  cou=np.sum(heavi(np.sum(np.abs(x),axis=-1)),axis=-1)

  lc=len(cou)
 
  cou=np.reshape(cou,(lc,1))

  eta=geteta(x)
  phi=getphi(x)


  cut=gs#how many particles to consider
  eta=eta[:,:cut]
  phi=phi[:,:cut]

  mx=np.mean(x,axis=1,keepdims=True)

  #meta=np.mean(eta,axis=-1)
  #mphi=np.mean(phi,axis=-1)

  meta=geteta(mx)[:,0]
  mphi=getphi(mx)[:,0]

  phi=np.transpose(np.array([phi[:,i]-mphi for i in range(cut)]))
  eta=np.transpose(np.array([eta[:,i]-meta for i in range(cut)]))

  radii=np.sqrt(eta**2+phi**2)[:,:20]

  radii %= 0.8#np.pi/2

  maxrad=np.reshape(np.max(radii,axis=-1),(lc,1))
  meanrad=np.reshape(np.mean(radii,axis=-1),(lc,1))

  return meanrad

  return np.concatenate((cou,maxrad,meanrad),axis=1)
  




