import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Layer, Input, Dense, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
import sys
import numpy as np
import matplotlib.pyplot as plt
import os
import math

from smult import *

from trainingon import trainingon


base=[1,3,9,27,9,3,1]
base=[1,0.00001]

def trainonwhich(trainingon):
  if "1" in trainingon:return 1
  if "0" in trainingon:return 0
  return -1



def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}

def getc():
  return np.sum([1 for q in os.listdir(".") if "seval" in q and ".npz" in q])

def loaddata(n=None):
  if n is None:n=getc()
  ret=[]
  for i in range(1,n+1):
    f=np.load("seval"+str(i)+".npz")
    ret.append(f["s"])
  ret=np.array(ret)
  ret=np.mean(ret,axis=-1)
  ret=np.transpose(ret)
  return ret,f["y"]


def getmodel(q,reg=None,act="relu",mean=1.0,seed=None,opt=None):
  if opt==None:opt=Adam(lr=0.01)
  if not seed is None:np.random.seed(seed)
  if not seed is None:tf.random.set_seed(seed)
  #inn=Input(shape=(28,28,1))
  inn=Input(shape=(q[0],))
  w=inn
  for aq in q[1:]:
    w=Dense(aq,activation=act,use_bias=False,kernel_initializer=keras.initializers.TruncatedNormal(),kernel_regularizer=reg)(w)
  w=smult(n=q[-1])([w])
  m=Model(inn,w,name="oneoff")
  zero=K.ones_like(w)*mean
  loss=mse(w,zero)
  loss=K.mean(loss)
  m.add_loss(loss)
  m.compile(Adam(lr=0.01))
  return m



def trainone(index,n,l,opt=None):
  cb=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=20,restore_best_weights=True),keras.callbacks.TerminateOnNaN()]
  if index is None:
    cb.append(keras.callbacks.ModelCheckpoint(f"msFep{n}.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
    cb.append(keras.callbacks.CSVLogger(f"history_sFep{n}.csv"))
  else:
    cb.append(keras.callbacks.ModelCheckpoint(f"multi/{index}/msFep{n}.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
    cb.append(keras.callbacks.CSVLogger(f"multi/{index}/history_sFep{n}.csv"))

  m=getmodel(l,opt=opt)
  m.summary()
  h=m.fit(t,None,
          epochs=500,
          batch_size=100,
          validation_split=0.25,
          verbose=1,
          callbacks=cb)



if __name__=="__main__":
  index=None
  try:
    if len(sys.argv)>1:index=int(sys.argv[1])
  except:pass

  n=getc()
  t,y=loaddata(n)


  if trainonwhich(trainingon)>0.5:
    t=t[np.where(y>0.5)]
  else:
    t=t[np.where(y<0.5)]

  #print(t.shape)
  mult=int(t.shape[-1])
  l=list([int(math.ceil(mult*ac)) for ac in base])

  ex=[1,5]
  if len(sys.argv)>2:ex=eval(sys.argv[2])

  #for i in range(ex[0],ex[1]+1):
  trainone(index=index,n=1,l=l,opt=Adam(lr=0.01))
  trainone(index=index,n=2,l=l,opt=Adam(lr=0.1))
  trainone(index=index,n=3,l=l,opt=Adam(lr=0.001))
  trainone(index=index,n=4,l=l,opt=tf.keras.optimizers.Adagrad(lr=0.01))
  trainone(index=index,n=5,l=l,opt=tf.keras.optimizers.Adamax(lr=0.01))
  trainone(index=index,n=6,l=l,opt=tf.keras.optimizers.Ftrl(learning_rate=0.01))
  trainone(index=index,n=7,l=l,opt=tf.keras.optimizers.Nadam(lr=0.01))
  trainone(index=index,n=8,l=l,opt=tf.keras.optimizers.RMSprop(lr=0.01))
  trainone(index=index,n=9,l=l,opt=tf.keras.optimizers.SGD(lr=0.01))

    #if len(sys.argv)>2 and index is None:
    #  os.system("python3 s2eval.py zero "+str(i))




















