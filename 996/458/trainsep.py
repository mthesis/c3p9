from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import sys

from agetsep import *



shallwrite=True

valsplit=0.1
keepsplit=0.3

f=np.load("bcode.npz")


x=f["p"]
trenn=int(valsplit*len(x))
trenn2=int((1-keepsplit)*len(x))

vx=x[:trenn]
tx=x[trenn:trenn2]

model=getsep()

cb=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=patience),
                   keras.callbacks.TerminateOnNaN()]
if shallwrite:
  cb.append(keras.callbacks.ModelCheckpoint("modelsepb.tf", monitor='val_loss', verbose=verbose, save_best_only=True,save_weights_only=True))
  cb.append(keras.callbacks.CSVLogger("history_sep.csv"))


# train the autoencoder
model.fit(tx,
        epochs=epochs,
        batch_size=batch_size,
        #)
        #validation_split=0.1,
        validation_data=(vx, None),
        verbose=verbose,
        callbacks=cb)










