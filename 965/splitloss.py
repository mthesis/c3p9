import numpy as np
import os



if os.path.isfile("bigevalb.npz"):
  f=np.load("bigevalb.npz")
else:
  print("loading evalb")
  f=np.load("evalb.npz")


c=f["c"]
p=f["p"]

n=c.shape[-1]


for i in range(n):
  ac=c[:,:,i]
  ap=p[:,:,i]
  ad=np.mean((ac-ap)**2)
  am=np.mean(np.abs(np.array([ac,ap])))
  cs=np.std([ac,ap])
  print("part",i,ad,am,cs)



d=np.mean((c-p)**2)

dnull=np.mean((c-np.zeros_like(p))**2)

mean=np.mean(c,axis=0)
dmean=np.mean((mean-c)**2)

sc=c.copy()
np.random.shuffle(sc)

drandom=np.mean((sc-c)**2)
#drandom=np.mean((np.roll(c,axis=0,shift=np.random.randint(len(c)))-c)**2)



print("my loss           : ",d)
print("null hypothesis   : ",dnull)
print("mean hypothesis   : ",dmean)
print("random hypothesis : ",drandom)



