from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import sys

from ageta import *







subset="_au0"
#subset="_short"+subset

shallshow=False
shallwrite=True

usevae=False


if len(sys.argv)>1:
  shallwrite=bool(int(sys.argv[1]))
if len(sys.argv)>2:
  if bool(int(sys.argv[2])):subset="_short"+subset

if shallwrite:
  for i in range(10):
    print("##################################################################")
  print("!!WRITING!!")
  for i in range(10):
    print("##################################################################")







def plot_results(models,
                 data,
                 batch_size=128):
    model_name="imgs"

    encoder, decoder = models
    x_test, y_test = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join(model_name, "vae_mean")
    # display a 2D plot of the digit classes in the latent space
    z_all = encoder.predict(x_test,
                                   batch_size=batch_size)
    z_mean=z_all[0]
    plt.figure(figsize=(12, 10))
    plt.scatter(z_mean[:, 0], z_mean[:, 1], c=y_test)
    plt.colorbar()
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig(filename+".png",format="png")
    plt.savefig(filename+".pdf",format="pdf")
    if shallshow:plt.show()


f=np.load("..//..//toptagdataref//train"+subset+".npz")
x=f["xb"][:n,:gs,:]
#y=f["y"][:n]
del f
vf=np.load("..//..//toptagdataref//val"+subset+".npz")
vx=vf["xb"][:vn,:gs,:]
vy=vf["y"][:vn]
del vf

if usevae:
  vae,encoder,decoder=getvae()
else:
  vae,encoder,decoder=getnvae()
models=(encoder,decoder)

p=encoder.predict(x)
print(x.shape)
for e in p:
  print(e.shape)

pp=decoder.predict(p)
print("!",pp.shape)



print(p[0,0])



