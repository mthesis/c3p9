import numpy as np
import os
from os import path as op
import matplotlib.pyplot as plt





fns=[q for q in os.listdir(".") if "rd" in q and ".npz" in q]

fs=[np.load(fn) for fn in fns]

ds=[f["ds"] for f in fs]

print(ds[0].shape)

ds=[d[0] for d in ds]
print(ds[0].shape)
nams=[q[2:q.find(".")] for q in fns]

for d,nam in zip(ds,nams):
  #if "_" in nam:continue
  if "gluon" in nam:continue
  plt.hist(d,bins=50,alpha=0.5,label=nam,density=True)

plt.legend()
plt.show()




