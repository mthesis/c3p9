import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd
import os

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *



from createmodel import objects as o
from adv2load import *
from ageta import *


import sys
scale=10
if len(sys.argv)>1:
  scale=int(sys.argv[1])

f=np.load("..//..//toptagdataref//val_fairer.npz")
x=f["xb"][:n,:gs,:]
y=f["y"][:n]
del f

#y=keras.utils.to_categorical(y,2)




#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               

def getbatch(m):


  model=getcompbt(m)



  ll=model.layers


  wei=[]
  nam=[]
  met=[]

  hasjumped=False
  for l in ll:
    if hasjumped==False:
      hasjumped=True
      continue
    if l._name.find("batch_norm")>-1:
      w=l.get_weights()
      ret=[]
      for e in w:
        toa=[]
        for ee in e:
          toa.append(float(ee))
        ret.append(toa)
      return model,ret
  return model,[]


def pred(f,i):
  model=aload(f)
  modelc,batch=getbatch(model)

  my=model.predict(x,verbose=1)
  mc=modelc.predict(x,verbose=1)
  ind=("%06d")%i
  np.savez_compressed("aeval/eval"+ind,x=x,y=y,p=my,c=mc,b=batch)



sfiles=os.listdir("amodels/")


files=[]
for fil in sfiles:
  if fil.find(".tf.index")>0:
    files.append("amodels/"+fil[:-6])

files.sort()

indices=np.arange(len(files))

print("found",len(files),"files")

files=files[::scale]
indices=indices[::scale]

print("rescaling to",len(files),"files")

import time
time.sleep(3)


for i in range(len(files)):
  pred(files[i],indices[i])

  for j in range(20):
    print("predicted",i,"of",len(files),files[i],indices[i])







