import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *



from createmodel import objects as o
from adv3load import *
from ageta import *
import sys
from trainingon import trainingon
from loaddata import *

index=None
if len(sys.argv)>1:
  index=int(sys.argv[1])


x=loadbigeval()
y=np.zeros(len(x))

#f=np.load("..//..//toptagdataref//val"+trainingon+".npz")
#x=f["xb"][:n,:gs,:]
#y=f["y"][:n]
#del f

#y=keras.utils.to_categorical(y,2)




#at the moment (?,gs=30,10)
#soll nachher (?,gs,gs+10+n) sein


#ich will hier: builder-layer-(cutter?)-feat-lbuilder-layer-feat-pool
#               

model=loadencoder(gs=gs,n=n,index=index)
#modelc=getcomp()


model.summary()





my=model.predict(x,verbose=1)

my=my[0]

print(my)
print(len(my),len(my[0]))

print(np.mean(my),np.std(my),np.max(my),np.min(my))



#mc=modelc.predict(x,verbose=1)
if index is None:
  np.savez_compressed("big2code",p=my)
else:
  np.savez_compressed(f"multi/{index}/big2code",p=my)




