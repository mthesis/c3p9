from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os

from createmodel import *







gs=100
n=600000#here needed to keep the number of samples const
vn=200000#here not really, but why not
out=2
epochs=1500#*0+1
verbose=1
patience=100

usemse=False

inn=1000

dd=[500,200,50,20,5]

def getsep():
  #inputs,comp,z_mean,z_log_var,mats,inputs2,outputs=createbothmodels(gs=gs,n=n,out=out)

  inputs=Input(shape=(inn,))

  d=inputs
  for ad in dd:
    d=Dense(ad)(d)
  outputs=d
  

  model = Model(inputs, outputs, name='vae')

  loss=K.mean(outputs,axis=0)**2
  loss+=(K.std(outputs,axis=0)-1)**2
  loss+=(K.mean(K.abs(outputs)-1,axis=0))**2


  loss=K.mean(loss)



  model.add_loss(loss)

  model.compile(optimizer="adam")

  return model














