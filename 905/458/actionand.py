import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





def R(self,x,c_const=1000.0,cut=0.5):
  return K.relu(c_const*(x-cut)+1)-K.relu(c_const*(x-c_const))
def doand(x,mode="prod",c_const=1000.0,cut=0.5):


  if mode=="and":
    ret=R(K.prod(x,axis=-1),c_const=c_const,cut=cut)
  if mode=="prod":
    ret=K.prod(x,axis=-1)
  if mode=="or":
    ret=R(K.sum(x,axis=-1),c_const=c_const,cut=cut)
  if mode=="sum":
    ret=K.sum(x,axis=-1)
      

  return ret






